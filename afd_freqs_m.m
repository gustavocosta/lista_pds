﻿%dB: Magnitude relativa em dB entre 0 e o máximo
%mag: Valor absoluto da magnitude enre 0 e o máximo
%pha: resposta da fase em radianos entre 0 e o valor máximo
%b:Coeficientes do polinômio do numerador de Ha(s)
%a:Coeficientes do polinômio do denominador de Ha(s)
%vmax: Máxima frequência em rad/s para a resposta desejada
function [b,a]=afd_freqs_m(b,a,wmax);
w=[0:1:500]*wmax/500;
H=freqs(b,a,w);
mag=abs(H);
db=20*log10((mag+eps)/max(mag));
pha= angle(H);

figure;
ax1 = subplot(3,1,1);
plot(ax1, w, mag);
title(ax1,'mag');
ax2 = subplot(3,1,2);
plot(ax2, w, db);
title(ax2,'db');
ax3 = subplot(3,1,3);
plot(ax3, w, pha);
title(ax3,'pha');
