﻿%b:Coeficientes do polinômio do numerador de Ha(s)
%a:Coeficientes do polinômio do denominador de Ha(s)
%Wp: Limite da frequência da banda passante em rad/s; Wp>0
%Ws: Limite da banda da frequência de corte em rad/s; Ws>Wp>0
%Rp: Ripple da banda passante em dB(Rp>0);
%Rs: Ripple da banda de rejeição em dB(Rs>0);
%As: Atenuação da banda passante em dB (As>0);
function [b,a]=afd_elip(Wp,Ws,Rp,As);
if Wp<=0
error('A banda passante deve ser maior que zero')
end
if Ws<=Wp
error('A frequencia de corte deve ser menor que a banda passante')
end
if(Rp<=0)|(As<0)
error('O Ripple da banda passante ou de Rejeição deve ser maior que zero')
end
ep=sqrt(10^(Rp/10)-1);
A=10^(As/20);
OmegaC=Wp;
k=Wp/Ws;
k1=ep/sqrt(A*A-1);
capk=ellipke([k.^2 1-k.^2]);
capk1=ellipke([(k.^2) 1-(k.^2)]);
N=ceil(capk(1)*capk1(2)/(capk(2)*capk1(1)));
fprintf('\n*** Elipitico Filtro de ordem = %2.0f\n',N);
OmegaC= Wp/((10^(Rp/10)-1)^(1/(2*N)))
[b,a]=u_elipap(N,Rp,As,OmegaC);