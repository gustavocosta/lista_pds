﻿%=============Butterworth====================
%[b, a] = u_buttap(3, 0.5);%1a
%[b, a] = u_buttap(4, 314.16);%1b
%[b, a] = afd_butt(0.2*pi, 0.3*pi, 1, 16);%2

%==============Chebyshev=====================
%[b, a] = afd_chb1(0.2*pi, 0.3*pi, 1, 16);
%[b, a] = afd_chb2(0.2*pi, 0.3*pi, 1, 16);

%==============Eliptico=====================
%[b, a] = afd_elip(0.2*pi, 0.3*pi, 1, 16)

%===============Digital=====================
%[b, a] = imp_invr([1, 1], [1, 5, 6], 0.1)%1

%[c, d] = afd_butt(0.2*pi, 0.3*pi, 1, 16);%2
%[c, d] = afd_chb1(0.2*pi, 0.3*pi, 1, 16);%3
%[c, d] = afd_chb2(0.2*pi, 0.3*pi, 1, 16);%4
%[c, d] = afd_elip(0.2*pi, 0.3*pi, 1, 16);%5
%[b, a] = imp_invr(c, d, 1)

%===========Transformação bilinear===========
%[b,a] = bilinear([1, 1], [1, 5, 6], 1.0)%1

%[c, d] = afd_butt(0.2*pi, 0.3*pi, 1, 16);%2
%[c, d] = afd_chb1(0.2*pi, 0.3*pi, 1, 16);%3
%[c, d] = afd_chb2(0.2*pi, 0.3*pi, 1, 16);%4
%[c, d] = afd_elip(0.2*pi, 0.3*pi, 1, 16);%5
%[b,a] = bilinear(c, d, 1.0)

%=============plotar graficos================
%afd_freqs_m(b, a, 5);

%============resposta ao impulso=============
%[ha,x,t] = impulse(b,a)
%plot(t, ha)