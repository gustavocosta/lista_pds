﻿%b:Coeficientes do polinômio do numerador de Ha(s)
%a:Coeficientes do polinômio do denominador de Ha(s)
%N:Ordem do filtro de Butterworth
%OmegaC= Frequência de corte do filtro em rad/s
function[b,a]=u_buttap(N,OmegaC);
[z,p,k]=buttap(N);
p=p*OmegaC;
k=k*OmegaC;
B=real(poly(z));
bo=k;
b=k*B;
a=real(poly(p));