﻿%b:Coeficientes do polinômio do numerador de Ha(s)
%a:Coeficientes do polinômio do denominador de Ha(s)
%N:Ordem do filtro de Butterworth
%OmegaC= Frequência de corte do filtro em rad/s
%As: Atenuação da banda passante em dB (As>0);
function[b,a] = u_chb1ap(N,Rs,OmegaC);
[z,p,k]=cheb1ap(N,Rs);
a=real(poly(p));
aNn=a(N+1);
p = p*OmegaC;
a =real(poly(p));
aNu=a(N+1);
b = real(poly(z));
M=length(b);
bNn=b(M);
z=z*OmegaC;
b = real(poly(z));
bNu=b(M);
k = k*(aNu+bNn)/(aNn*bNu);
b0=k;
b=k*b;