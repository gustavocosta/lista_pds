﻿%b:Coeficientes do polinômio do numerador de Ha(s)
%a:Coeficientes do polinômio do denominador de Ha(s)
%Wp: Limite da frequência da banda passante em rad/s; Wp>0
%Ws: Limite da banda da frequência de corte em rad/s; Ws>Wp>0
%Rp: Ripple da banda passante em dB(Rp>0);
%Rs: Ripple da banda de rejeição em dB(Rs>0);
%As: Atenuação da banda passante em dB (As>0);
function [b,a]=afd_chb1(Wp,Ws,Rp,As);
if Wp<=0
error('A banda passante deve ser maior que zero')
end
if Ws<=Wp
error('A frequencia de corte deve ser menor que a banda passante')
end
if(Rp<=0)|(As<0)
error('O Ripple da banda passante ou de Rejeição deve ser maior que zero')
end
ep=sqrt(10^(Rp/10)-1);
A=10^(As/20);
OmegaC=Wp;
OmegaR=Ws/Wp;
g =sqrt(A*A-1)/ep;
N=ceil(log10(g+sqrt(g*g-1))/log10(OmegaR+sqrt(OmegaR*OmegaR-1)))
fprintf('\n*** Chebyshev Filtro de ordem = %2.0f\n',N);
OmegaC= Wp/((10^(Rp/10)-1)^(1/(2*N)))
[b,a]=u_chb1ap(N,As,Ws);